import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import Link from 'gatsby-link';
// import logo from '../styles/logo/logo.png';
import logo from '../styles/logo/devkozak_light.svg';
import logo_mini from '../styles/logo/logo_mini.png';

// import Header from '../components/header';
// import Sidebar from '../components/sidebar';
// import LeftBar from '../components/leftbar';
// import TopBar from '../components/topbar';
import './index.css';
import '../styles/main.scss';

const Layout = ({ children, data }) => (
  <div className="container">
    <Helmet
      title={data.site.siteMetadata.title}
      meta={[
        { name: 'description', content: 'Sample' },
        { name: 'keywords', content: 'sample, something' },
        //  {name: 'viewport', content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'}
      ]}
      link={[
        { rel: 'shortcut icon', type: 'image/png', href: `${logo_mini}` }
      ]}
    />
    <div className="header">
      <div className="header__logo">
        <Link to="/">
          <img src={logo} alt="devkozak" />
        </Link>
      </div>
      <div className="header__menu">
        <ul className="nav-list">
          <li className="nav-item">
            <Link to="/">Blog</Link>
          </li>
          <li className="nav-item">
            <Link to="/">About</Link>
          </li>
          <li className="nav-item">
            <Link to="/">Contact</Link>
          </li>
        </ul>
      </div>
    </div>
    <div className="content">
      {children()}
      <footer>
        <p>©2018 Dawid Kozak</p>
      </footer>
    </div>
  </div>
);

Layout.propTypes = {
  children: PropTypes.func,
};

export default Layout;

export const query = graphql`
  query SiteTitleQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`;
