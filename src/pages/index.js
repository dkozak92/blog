import React from 'react';
import Link from 'gatsby-link';
import Helmet from 'react-helmet';

export default function Index({ data }) {
  const { edges: posts } = data.allMarkdownRemark;
  return (
    // <div className="content__element">
    // </div>
    <div className="posts">
      {posts.filter(post => post.node.frontmatter.title.length > 0).map(({ node: post }) => {
        return (
          <Link to={post.frontmatter.path} key={post.id}>
            <div className="posts__element">
              <div className="posts__element--meta">
                <span>{post.frontmatter.date}</span>
              </div>
              <h1>{post.frontmatter.title}</h1>
              <p>{post.excerpt}</p>
            </div>
          </Link>
        );
      })}
    </div>
  );
}
export const pageQuery = graphql`
  query IndexQuery {
    allMarkdownRemark(sort: { order: DESC, fields: [frontmatter___date] }) {
      edges {
        node {
          excerpt(pruneLength: 250)
          id
          frontmatter {
            title
            date(formatString: "MMMM DD, YYYY")
            path
          }
        }
      }
    }
  }
`;
